package by.alex.fastfoodrestraunt.repository;

import by.alex.fastfoodrestraunt.entity.FoodType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@ExtendWith(SpringExtension.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class FoodTypeRepositoryTest {

    @Autowired
    private FoodTypeRepository foodTypeRepository;
    private FoodType foodType;
    private FoodType foodType1;

    @BeforeEach
    void setUp() {
        foodType = FoodType.builder()
                .id(UUID.randomUUID())
                .name("Hamburger")
                .build();
        foodType1 = FoodType.builder()
                .id(UUID.randomUUID())
                .name("Pizza")
                .build();
    }

    @AfterEach
    void tearDown() {
        foodTypeRepository.deleteAll();
        foodType = null;
    }

    @Test
    public void testCreationOfFoodType() {
        FoodType foodType = foodTypeRepository.save(this.foodType);
        assertEquals(this.foodType, foodType);
    }

    @Test
    public void testCreationOfListOfProductTypes() {
        List<FoodType> foodTypes = foodTypeRepository.saveAll(Arrays.asList(foodType, foodType1));
        assertNotNull(foodTypes);
        assertEquals(foodType, foodTypes.get(0));
        assertEquals(foodType1, foodTypes.get(1));
    }

    @Test
    public void testGettingOfFoodTypeById() {
        foodTypeRepository.save(foodType);
        FoodType foodTypeFromDb = foodTypeRepository.findById(foodType.getId()).orElse(null);
        assertNotNull(foodTypeFromDb);
        assertEquals(foodType, foodTypeFromDb);
    }

    @Test
    public void testGettingOfListOfFoodTypesByIds() {
        foodTypeRepository.saveAll(Arrays.asList(foodType, foodType1));
        List<UUID> ids = Arrays.asList(foodType.getId(), foodType1.getId());
        List<FoodType> foodTypes = foodTypeRepository.findAllById(ids);
        assertNotNull(foodTypes);
        assertEquals(foodType, foodTypes.get(0));
        assertEquals(foodType1, foodTypes.get(1));
    }

    @Test
    public void testUpdatingOfFoodType() {
        foodTypeRepository.save(foodType);
        foodType.setName("Pizza");
        foodTypeRepository.save(foodType);
        FoodType foodTypeFromDb = foodTypeRepository.findById(foodType.getId()).orElse(null);
        assertNotNull(foodTypeFromDb);
        assertEquals(foodType, foodTypeFromDb);
    }

    @Test
    public void testDeletingOfFoodType() {
        foodTypeRepository.save(foodType);
        foodTypeRepository.delete(foodType);
        FoodType foodTypeFromDb = foodTypeRepository.findById(foodType.getId()).orElse(null);
        assertNull(foodTypeFromDb);
    }
}