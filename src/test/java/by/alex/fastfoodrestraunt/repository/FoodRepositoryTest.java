package by.alex.fastfoodrestraunt.repository;

import by.alex.fastfoodrestraunt.entity.Food;
import by.alex.fastfoodrestraunt.entity.FoodType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@ExtendWith(SpringExtension.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class FoodRepositoryTest {

    @Autowired
    private FoodRepository foodRepository;
    @Autowired
    private FoodTypeRepository foodTypeRepository;
    private Food food;
    private FoodType foodType;

    @BeforeEach
    void setUp() {
        foodType = FoodType.builder()
                .id(UUID.randomUUID())
                .name("Hamburger")
                .build();
        food = Food.builder()
                .id(UUID.randomUUID())
                .ingredients(Arrays.asList("Bread", "Meat", "Cheese", "Ketchup", "Salad", "Tomatoes"))
                .calories(252.0)
                .weight(250.0)
                .type(foodType)
                .build();
    }

    @AfterEach
    void tearDown() {
        food = null;
        foodType = null;
    }

    @Test
    public void testCreationOfFoodWithoutCreatedFoodType() {
        assertThrows(JpaObjectRetrievalFailureException.class, () -> foodRepository.save(food));
    }

    @Test
    public void testCreationOfFoodWithCreatedFoodType() {
        foodTypeRepository.save(foodType);
        Food foodFromDb = foodRepository.save(food);
        assertNotNull(foodFromDb);
        assertEquals(food, foodFromDb);
    }

    @Test
    public void testUpdateFoodWithNotValidFoodTypeId() {
        foodTypeRepository.save(foodType);
        foodRepository.save(food);
        foodType.setId(UUID.randomUUID());
        assertThrows(JpaObjectRetrievalFailureException.class, () -> foodRepository.save(food));
    }

    @Test
    public void testUpdateFoodWithNotValidFoodTypeName() {
        foodTypeRepository.save(foodType);
        foodRepository.save(food);
        foodType.setName("Pizza");
        Food foodFromDb = foodRepository.save(food);
        assertNotEquals(food, foodFromDb);
    }

    @Test
    public void testDeletionOfFoodWithoutFoodType() {
        foodTypeRepository.save(foodType);
        foodRepository.save(food);
        foodRepository.delete(food);
        FoodType foodTypeFromDb = foodTypeRepository.findById(foodType.getId()).orElse(null);
        assertNotNull(foodTypeFromDb);
        assertEquals(foodType, foodTypeFromDb);
    }
}