package by.alex.fastfoodrestraunt.controller.v1;

import by.alex.fastfoodrestraunt.dto.FoodTypeDTO;
import by.alex.fastfoodrestraunt.entity.FoodType;
import by.alex.fastfoodrestraunt.exception.FoodTypeNotFoundException;
import by.alex.fastfoodrestraunt.service.FoodTypeService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FoodTypeControllerV1Test {

    @Mock
    private FoodTypeService foodTypeService;

    @Spy
    private ModelMapper modelMapper = new ModelMapper();

    @InjectMocks
    private FoodTypeControllerV1 foodTypeController;
    private FoodType foodType;
    private FoodTypeDTO foodTypeDTO;

    @BeforeEach
    void setUp() {
        foodTypeDTO = FoodTypeDTO.builder()
                .id(UUID.randomUUID())
                .name("Hamburger")
                .build();
        foodType = new ModelMapper().map(foodTypeDTO, FoodType.class);
    }

    @AfterEach
    void tearDown() {
        foodTypeDTO = null;
        foodType = null;
    }

    @Test
    public void createdFoodTypeShouldBeEqualToInitial() {
        when(foodTypeService.createFoodType(foodType)).thenReturn(foodType);
        FoodType foodTypeFromService = foodTypeController.createFoodType(foodTypeDTO);
        assertEquals(foodType, foodTypeFromService);
    }

    @Test
    public void methodGetFoodTypeByIdShouldThrowAnExceptionForUnknownFoodTypeIds() throws FoodTypeNotFoundException {
        when(foodTypeService.getFoodTypeById(any())).thenThrow(FoodTypeNotFoundException.class);
        assertThrows(FoodTypeNotFoundException.class, () -> foodTypeController.getFoodTypeById(foodType.getId()));
    }

    @Test
    public void foundFoodTypeShouldBeEqualToExpected() throws FoodTypeNotFoundException {
        when(foodTypeService.getFoodTypeById(foodType.getId())).thenReturn(foodType);
        FoodType foodTypeFromService = foodTypeController.getFoodTypeById(foodTypeDTO.getId());
        assertEquals(foodType, foodTypeFromService);
    }

    @Test
    public void updatedFoodTypeShouldBeEqualToRequested() {
        when(foodTypeService.updateFoodType(foodType)).thenReturn(foodType);
        FoodType foodTypeFromService = foodTypeController.updateFoodType(foodTypeDTO);
        assertEquals(foodType, foodTypeFromService);
    }

    @Test
    public void deletionShouldDoNothing() {
        foodTypeController.deleteFoodType(foodTypeDTO.getId());
    }
}