package by.alex.fastfoodrestraunt.controller.v1;

import by.alex.fastfoodrestraunt.dto.FoodDTO;
import by.alex.fastfoodrestraunt.entity.Food;
import by.alex.fastfoodrestraunt.entity.FoodType;
import by.alex.fastfoodrestraunt.exception.FoodNotFoundException;
import by.alex.fastfoodrestraunt.service.FoodService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.Arrays;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FoodControllerV1Test {

    @Mock
    private FoodService foodService;
    @Spy
    private ModelMapper modelMapper;

    @InjectMocks
    private FoodControllerV1 foodController;

    private FoodType foodType;
    private Food food;
    private FoodDTO foodDTO;

    @BeforeEach
    void setUp() {
        foodType = FoodType.builder()
                .id(UUID.randomUUID())
                .name("Hamburger")
                .build();
        food = Food.builder()
                .id(UUID.randomUUID())
                .type(foodType)
                .weight(243.2)
                .calories(432.6)
                .ingredients(Arrays.asList("Bread", "Chicken", "Sous", "Cheese"))
                .build();
        foodDTO = modelMapper.map(food, FoodDTO.class);
    }

    @AfterEach
    void tearDown() {
        foodType = null;
        food = null;
    }

    @Test
    public void createdFoodShouldBeEqualToInitial() {
        when(foodService.createFood(food)).thenReturn(food);
        Food foodFromService = foodController.createFood(foodDTO);
        assertEquals(food, foodFromService);
    }

    @Test
    public void exceptionShouldBeThrownIfFoodIsNotFound() throws FoodNotFoundException {
        when(foodService.getFoodById(any())).thenThrow(FoodNotFoundException.class);
        assertThrows(FoodNotFoundException.class, () -> foodController.getFoodById(food.getId()));
    }

    @Test
    public void expectedFoodShouldBeReturned() throws FoodNotFoundException {
        when(foodService.getFoodById(food.getId())).thenReturn(food);
        Food foodFromService = foodController.getFoodById(food.getId());
        assertEquals(food, foodFromService);
    }

    @Test
    public void updatedFoodShouldBeEqualToExpectedToUpdate() {
        when(foodService.updateFood(food)).thenReturn(food);
        Food foodFromService = foodController.updateFood(foodDTO);
        assertEquals(food, foodFromService);
    }

    @Test
    public void dummyTestForDeletion() {
        foodController.deleteFood(food.getId());
    }
}