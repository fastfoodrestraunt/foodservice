package by.alex.fastfoodrestraunt.service.impl;

import by.alex.fastfoodrestraunt.entity.FoodType;
import by.alex.fastfoodrestraunt.exception.FoodTypeNotFoundException;
import by.alex.fastfoodrestraunt.repository.FoodTypeRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FoodTypeServiceImplTest {

    @Mock
    private FoodTypeRepository foodTypeRepository;

    @InjectMocks
    @Autowired
    private FoodTypeServiceImpl foodTypeService;

    private FoodType foodType;

    @BeforeEach
    public void setUp() {
        foodType = FoodType.builder()
                .id(UUID.randomUUID())
                .name("Hamburger")
                .build();
    }

    @AfterEach
    public void tearDown() {
        foodType = null;
    }

    @Test
    public void createdFoodTypeShouldBeEqualToInitial() {
        when(foodTypeRepository.save(foodType)).thenReturn(foodType);
        FoodType foodTypeFromDb = foodTypeService.createFoodType(foodType);
        assertEquals(foodType, foodTypeFromDb);
    }

    @Test
    public void ifFoodTypeIsNotFoundMethodShouldThrowAnException() {
        when(foodTypeRepository.findById(any())).thenReturn(Optional.empty());
        assertThrows(FoodTypeNotFoundException.class, () -> foodTypeService.getFoodTypeById(foodType.getId()));
    }

    @Test
    public void methodGetFoodTypeByIdShouldReturnActualFoodType() throws FoodTypeNotFoundException {
        when(foodTypeRepository.findById(foodType.getId())).thenReturn(Optional.of(foodType));
        FoodType foodTypeFromDb = foodTypeService.getFoodTypeById(foodType.getId());
        assertEquals(foodType, foodTypeFromDb);
    }

    @Test
    public void methodUpdateFoodTypeShouldReturnUpdatedFoodType() {
        when(foodTypeRepository.save(foodType)).thenReturn(foodType);
        FoodType foodTypeFromDb = foodTypeService.updateFoodType(foodType);
        assertEquals(foodType, foodTypeFromDb);
    }

    @Test
    public void methodDeleteFoodTypeShouldReturnTrueForDeletedFoodType() {
        when(foodTypeRepository.findById(foodType.getId())).thenReturn(Optional.of(foodType));
        doNothing().when(foodTypeRepository).delete(foodType);
        assertTrue(foodTypeService.deleteFoodType(foodType.getId()));
    }

    @Test
    public void deletionOfUnknownFoodTypeShouldThrowAnException() {
        when(foodTypeRepository.findById(any())).thenReturn(Optional.empty());
        assertFalse(foodTypeService.deleteFoodType(foodType.getId()));
    }

}