package by.alex.fastfoodrestraunt.service.impl;

import by.alex.fastfoodrestraunt.entity.Food;
import by.alex.fastfoodrestraunt.entity.FoodType;
import by.alex.fastfoodrestraunt.exception.FoodNotFoundException;
import by.alex.fastfoodrestraunt.repository.FoodRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FoodServiceImplTest {

    @Mock
    private FoodRepository foodRepository;

    @Autowired
    @InjectMocks
    private FoodServiceImpl foodService;
    private Food food;
    private FoodType foodType;

    @BeforeEach
    public void setUp() {
        foodType = FoodType.builder()
                .id(UUID.randomUUID())
                .name("Hamburger")
                .build();
        food = Food.builder()
                .id(UUID.randomUUID())
                .type(foodType)
                .weight(250.0)
                .calories(230.2)
                .ingredients(Arrays.asList("Bread", "Chicken", "Tomato", "Cheese", "Sous"))
                .build();
    }

    @AfterEach
    public void tearDown() {
        food = null;
        foodType = null;
    }

    @Test
    public void createdFoodShouldBeEqualToInitialFood() {
        when(foodRepository.save(any())).thenReturn(food);
        Food foodFromService = foodService.createFood(food);
        assertEquals(food, foodFromService);
    }

    @Test
    public void methodGetFoodByIdShouldReturnEntity() throws FoodNotFoundException {
        when(foodRepository.findById(any())).thenReturn(Optional.of(food));
        Food foodFromService = foodService.getFoodById(food.getId());
        assertEquals(food, foodFromService);
    }

    @Test
    public void methodGetFoodByIdShouldThrowExceptionForUnknownIds() {
        when(foodRepository.findById(any())).thenReturn(Optional.empty());
        assertThrows(FoodNotFoundException.class, () -> foodService.getFoodById(food.getId()));
    }

    @Test
    public void updatedFoodShouldBeEqualToInitial() {
        when(foodRepository.save(any())).thenReturn(food);
        Food foodFromService = foodService.updateFood(food);
        assertEquals(food, foodFromService);
    }

    @Test
    public void methodDeleteFoodShouldThrowExceptionsIfEntityIsNotFound() {
        boolean result = foodService.deleteFood(food.getId());
        assertFalse(result);
    }

    @Test
    public void methodDeleteFoodShouldDeleteFood() {
        when(foodRepository.findById(any())).thenReturn(Optional.of(food));
        boolean result = foodService.deleteFood(food.getId());
        assertTrue(result);
    }
}