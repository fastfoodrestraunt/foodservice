package by.alex.fastfoodrestraunt.controller.v1;

import by.alex.fastfoodrestraunt.entity.FoodType;
import by.alex.fastfoodrestraunt.dto.FoodTypeDTO;
import by.alex.fastfoodrestraunt.exception.FoodTypeNotFoundException;
import by.alex.fastfoodrestraunt.service.FoodTypeService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("api/v1/food-types")
@RequiredArgsConstructor
public class FoodTypeControllerV1 {

    private final FoodTypeService foodTypeService;
    private final ModelMapper modelMapper;

    @PostMapping
    public FoodType createFoodType(@RequestBody FoodTypeDTO foodTypeDTO) {
        FoodType foodType = modelMapper.map(foodTypeDTO, FoodType.class);
        return foodTypeService.createFoodType(foodType);
    }

    @GetMapping("{id}")
    public FoodType getFoodTypeById(@PathVariable UUID id) throws FoodTypeNotFoundException {
        return foodTypeService.getFoodTypeById(id);
    }

    @PutMapping
    public FoodType updateFoodType(@RequestBody FoodTypeDTO foodTypeDTO) {
        FoodType foodType = modelMapper.map(foodTypeDTO, FoodType.class);
        return foodTypeService.updateFoodType(foodType);
    }

    @DeleteMapping("{id}")
    public void deleteFoodType(@PathVariable UUID id) {
        foodTypeService.deleteFoodType(id);
    }
}
