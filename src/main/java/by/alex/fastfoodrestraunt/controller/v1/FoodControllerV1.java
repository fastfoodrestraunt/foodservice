package by.alex.fastfoodrestraunt.controller.v1;

import by.alex.fastfoodrestraunt.entity.Food;
import by.alex.fastfoodrestraunt.dto.FoodDTO;
import by.alex.fastfoodrestraunt.exception.FoodNotFoundException;
import by.alex.fastfoodrestraunt.service.FoodService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("api/v1/foods")
@RequiredArgsConstructor
public class FoodControllerV1 {

    private final FoodService foodService;
    private final ModelMapper modelMapper;

    @PostMapping
    public Food createFood(@RequestBody FoodDTO foodDTO) {
        Food food = modelMapper.map(foodDTO, Food.class);
        return foodService.createFood(food);
    }

    @GetMapping("{id}")
    public Food getFoodById(@PathVariable UUID id) throws FoodNotFoundException {
        return foodService.getFoodById(id);
    }

    @PutMapping
    public Food updateFood(@RequestBody FoodDTO foodDTO) {
        Food food = modelMapper.map(foodDTO, Food.class);
        return foodService.updateFood(food);
    }

    @DeleteMapping("{id}")
    public void deleteFood(@PathVariable UUID id) {
        foodService.deleteFood(id);
    }
}
