package by.alex.fastfoodrestraunt.service;

import by.alex.fastfoodrestraunt.entity.Food;
import by.alex.fastfoodrestraunt.exception.FoodNotFoundException;

import java.util.UUID;

public interface FoodService {

    Food createFood(Food food);

    Food getFoodById(UUID id) throws FoodNotFoundException;

    Food updateFood(Food food);

    boolean deleteFood(UUID id);
}
