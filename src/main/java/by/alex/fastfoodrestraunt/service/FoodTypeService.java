package by.alex.fastfoodrestraunt.service;

import by.alex.fastfoodrestraunt.entity.FoodType;
import by.alex.fastfoodrestraunt.exception.FoodTypeNotFoundException;

import java.util.UUID;

public interface FoodTypeService {

    FoodType createFoodType(FoodType foodType);

    FoodType getFoodTypeById(UUID id) throws FoodTypeNotFoundException;

    FoodType updateFoodType(FoodType foodType);

    boolean deleteFoodType(UUID id);
}
