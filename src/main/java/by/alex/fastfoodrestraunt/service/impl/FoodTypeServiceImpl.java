package by.alex.fastfoodrestraunt.service.impl;

import by.alex.fastfoodrestraunt.entity.FoodType;
import by.alex.fastfoodrestraunt.exception.FoodTypeNotFoundException;
import by.alex.fastfoodrestraunt.repository.FoodTypeRepository;
import by.alex.fastfoodrestraunt.service.FoodTypeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class FoodTypeServiceImpl implements FoodTypeService {

    private final FoodTypeRepository foodTypeRepository;

    @Override
    @Transactional
    public FoodType createFoodType(FoodType foodType) {
        return foodTypeRepository.save(foodType);
    }

    @Override
    @Transactional
    public FoodType getFoodTypeById(UUID id) throws FoodTypeNotFoundException {
        return foodTypeRepository.findById(id).orElseThrow(new FoodTypeNotFoundException("Food type with id " + id + " is not found"));
    }

    @Override
    @Transactional
    public FoodType updateFoodType(FoodType foodType) {
        return foodTypeRepository.save(foodType);
    }

    @Override
    @Transactional
    public boolean deleteFoodType(UUID id) {
        try {
            foodTypeRepository.delete(getFoodTypeById(id));
            return true;
        } catch (FoodTypeNotFoundException foodTypeNotFoundException) {
            return false;
        }
    }
}
