package by.alex.fastfoodrestraunt.service.impl;

import by.alex.fastfoodrestraunt.entity.Food;
import by.alex.fastfoodrestraunt.exception.FoodNotFoundException;
import by.alex.fastfoodrestraunt.repository.FoodRepository;
import by.alex.fastfoodrestraunt.service.FoodService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class FoodServiceImpl implements FoodService {

    private final FoodRepository foodRepository;

    @Override
    @Transactional
    public Food createFood(Food food) {
        return foodRepository.save(food);
    }

    @Override
    @Transactional
    public Food getFoodById(UUID id) throws FoodNotFoundException {
        return foodRepository.findById(id).orElseThrow(new FoodNotFoundException("Food with id " + id + " is not found"));
    }

    @Override
    @Transactional
    public Food updateFood(Food food) {
        return foodRepository.save(food);
    }

    @Override
    @Transactional
    public boolean deleteFood(UUID id) {
        try {
            foodRepository.delete(getFoodById(id));
            return true;
        } catch (FoodNotFoundException e) {
            return false;
        }

    }
}
