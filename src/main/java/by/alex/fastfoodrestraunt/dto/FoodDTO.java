package by.alex.fastfoodrestraunt.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FoodDTO {

    private UUID id;
    private Double weight;
    private Double calories;
    private List<String> ingredients;
    private FoodTypeDTO type;
}
