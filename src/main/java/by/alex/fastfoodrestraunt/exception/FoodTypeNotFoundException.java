package by.alex.fastfoodrestraunt.exception;

import java.util.function.Supplier;

public class FoodTypeNotFoundException extends Exception implements Supplier<FoodTypeNotFoundException> {

    public FoodTypeNotFoundException(String message) {
        super(message);
    }

    @Override
    public FoodTypeNotFoundException get() {
        return this;
    }
}
