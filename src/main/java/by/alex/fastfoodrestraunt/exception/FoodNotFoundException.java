package by.alex.fastfoodrestraunt.exception;

import java.util.function.Supplier;

public class FoodNotFoundException extends Exception implements Supplier<FoodNotFoundException> {

    public FoodNotFoundException(String message) {
        super(message);
    }

    @Override
    public FoodNotFoundException get() {
        return this;
    }
}
