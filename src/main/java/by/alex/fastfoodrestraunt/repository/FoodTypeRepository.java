package by.alex.fastfoodrestraunt.repository;

import by.alex.fastfoodrestraunt.entity.FoodType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.UUID;

@Repository
public interface FoodTypeRepository extends JpaRepository<FoodType, UUID> {
}
