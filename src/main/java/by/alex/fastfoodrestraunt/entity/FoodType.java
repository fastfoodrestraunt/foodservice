package by.alex.fastfoodrestraunt.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;
import java.util.UUID;

@Component
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter
@Setter
public class FoodType {

    @Id
    private UUID id;
    private String name;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FoodType)) return false;
        FoodType foodType = (FoodType) o;
        return Objects.equals(id, foodType.id) && Objects.equals(name, foodType.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
