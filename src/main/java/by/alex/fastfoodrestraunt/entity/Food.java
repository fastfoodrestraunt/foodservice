package by.alex.fastfoodrestraunt.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.stereotype.Component;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Component
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Getter
@Setter
public class Food {

    @Id
    private UUID id;

    private Double weight;
    private Double calories;
    @Column
    @ElementCollection
    private List<String> ingredients;

    @ManyToOne
    private FoodType type;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Food)) return false;
        Food food = (Food) o;
        return Objects.equals(id, food.id) && Objects.equals(weight, food.weight) && Objects.equals(calories, food.calories) && Objects.equals(ingredients, food.ingredients) && Objects.equals(type, food.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, weight, calories, ingredients, type);
    }
}
